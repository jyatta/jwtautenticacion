package com.yattasoft.seguridad.models.service;

import java.util.List;

import com.yattasoft.seguridad.models.entity.Ms_tblUsuarios;

public interface Ms_tblUsuariosService {
	public List<Ms_tblUsuarios> findAllMs_tblUsuarios();

	public Ms_tblUsuarios findbyIDMs_tblUsuarios(int id);

	public Ms_tblUsuarios getMs_tblUsuariobysLogin_id(String sLogin_id);
}
