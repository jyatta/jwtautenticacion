package com.yattasoft.seguridad.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.yattasoft.seguridad.models.entity.Ms_tblUsuarios;

public interface Ms_tblUsuariosDao extends CrudRepository<Ms_tblUsuarios, Integer> {
	@Query("SELECT usuario FROM Ms_tblUsuarios usuario WHERE usuario.sLogin_id = ?1 ")
	Ms_tblUsuarios getMs_tblUsuariobysLogin_id(String sLogin_id);

}
