package com.yattasoft.seguridad.models.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ms_tblUsuarios")
public class Ms_tblUsuarios implements Serializable {
	@Id
	@Column(name = "lUsuario_id", insertable = false, updatable = false)
	private int lUsuario_id;
	@Column(name = "sLogin_id", insertable = false, updatable = false)
	private String sLogin_id;
	@Column(name = "sPassword_id", insertable = false, updatable = false)
	private String sPassword_id;
	@Column(name = "iEstado_fl", insertable = false, updatable = false)
	private String iEstado_fl;
	@Column(name = "lPFisica_id", insertable = false, updatable = false)
	private int lPFisica_id;
	@Column(name = "iPerfil_id", insertable = false, updatable = false)
	private int iPerfil_id;
	@Column(name = "iForgotQuestion_id", insertable = false, updatable = false)
	private String iForgotQuestion_id;
	@Column(name = "sForgotAnswer_desc", insertable = false, updatable = false)
	private String sForgotAnswer_desc;
	@Column(name = "iType_id", insertable = false, updatable = false)
	private String iType_id;
	@Column(name = "iLogged_fl", insertable = false, updatable = false)
	private String iLogged_fl;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dtValidUpTo_date", nullable = true)
	private Calendar dtValidUpTo_date;
	@Column(name = "iBadLogin_qty", insertable = false, updatable = false)
	private int iBadLogin_qty;
	@Column(name = "iEliminado_fl", insertable = false, updatable = false)
	private String iEliminado_fl;
	@Column(name = "sCreated_by", insertable = false, updatable = false)
	private String sCreated_by;
	@Column(name = "iConcurrencia_id", insertable = false, updatable = false)
	private int iConcurrencia_id;
	private static final long serialVersionUID = 2057427707480370323L;
	public int getlUsuario_id() {
		return lUsuario_id;
	}
	public void setlUsuario_id(int lUsuario_id) {
		this.lUsuario_id = lUsuario_id;
	}
	public String getsLogin_id() {
		return sLogin_id;
	}
	public void setsLogin_id(String sLogin_id) {
		this.sLogin_id = sLogin_id;
	}
	public String getsPassword_id() {
		return sPassword_id;
	}
	public void setsPassword_id(String sPassword_id) {
		this.sPassword_id = sPassword_id;
	}
	public String getiEstado_fl() {
		return iEstado_fl;
	}
	public void setiEstado_fl(String iEstado_fl) {
		this.iEstado_fl = iEstado_fl;
	}
	public int getlPFisica_id() {
		return lPFisica_id;
	}
	public void setlPFisica_id(int lPFisica_id) {
		this.lPFisica_id = lPFisica_id;
	}
	public int getiPerfil_id() {
		return iPerfil_id;
	}
	public void setiPerfil_id(int iPerfil_id) {
		this.iPerfil_id = iPerfil_id;
	}
	public String getiForgotQuestion_id() {
		return iForgotQuestion_id;
	}
	public void setiForgotQuestion_id(String iForgotQuestion_id) {
		this.iForgotQuestion_id = iForgotQuestion_id;
	}
	public String getsForgotAnswer_desc() {
		return sForgotAnswer_desc;
	}
	public void setsForgotAnswer_desc(String sForgotAnswer_desc) {
		this.sForgotAnswer_desc = sForgotAnswer_desc;
	}
	public String getiType_id() {
		return iType_id;
	}
	public void setiType_id(String iType_id) {
		this.iType_id = iType_id;
	}
	public String getiLogged_fl() {
		return iLogged_fl;
	}
	public void setiLogged_fl(String iLogged_fl) {
		this.iLogged_fl = iLogged_fl;
	}
	public Calendar getDtValidUpTo_date() {
		return dtValidUpTo_date;
	}
	public void setDtValidUpTo_date(Calendar dtValidUpTo_date) {
		this.dtValidUpTo_date = dtValidUpTo_date;
	}
	public int getiBadLogin_qty() {
		return iBadLogin_qty;
	}
	public void setiBadLogin_qty(int iBadLogin_qty) {
		this.iBadLogin_qty = iBadLogin_qty;
	}
	public String getiEliminado_fl() {
		return iEliminado_fl;
	}
	public void setiEliminado_fl(String iEliminado_fl) {
		this.iEliminado_fl = iEliminado_fl;
	}
	public String getsCreated_by() {
		return sCreated_by;
	}
	public void setsCreated_by(String sCreated_by) {
		this.sCreated_by = sCreated_by;
	}
	public int getiConcurrencia_id() {
		return iConcurrencia_id;
	}
	public void setiConcurrencia_id(int iConcurrencia_id) {
		this.iConcurrencia_id = iConcurrencia_id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
}
