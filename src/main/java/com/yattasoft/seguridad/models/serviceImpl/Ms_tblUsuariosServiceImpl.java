package com.yattasoft.seguridad.models.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yattasoft.seguridad.models.dao.Ms_tblUsuariosDao;
import com.yattasoft.seguridad.models.entity.Ms_tblUsuarios;
import com.yattasoft.seguridad.models.service.Ms_tblUsuariosService;

@Service
public class Ms_tblUsuariosServiceImpl implements Ms_tblUsuariosService {

	@Autowired
	private Ms_tblUsuariosDao Ms_tblUsuariosDao;

	@Override
	@Transactional(readOnly = true)
	public List<Ms_tblUsuarios> findAllMs_tblUsuarios() {
		return (List<Ms_tblUsuarios>) Ms_tblUsuariosDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Ms_tblUsuarios findbyIDMs_tblUsuarios(int id) {
		return Ms_tblUsuariosDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Ms_tblUsuarios getMs_tblUsuariobysLogin_id(String sLogin_id) {
		return Ms_tblUsuariosDao.getMs_tblUsuariobysLogin_id(sLogin_id);
	}
}
