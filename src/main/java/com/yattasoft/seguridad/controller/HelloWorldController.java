package com.yattasoft.seguridad.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@PostMapping(value = "/hello")
	public String helloWorld(@RequestParam(value = "name", defaultValue = "World") String name) {
		return "Hello " + name + "!!";
	}
}
