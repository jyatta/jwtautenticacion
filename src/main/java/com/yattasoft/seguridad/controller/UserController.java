package com.yattasoft.seguridad.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yattasoft.seguridad.dto.User;
import com.yattasoft.seguridad.models.entity.Ms_tblUsuarios;
import com.yattasoft.seguridad.models.service.Ms_tblUsuariosService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class UserController {
	public static Logger LOGGER = Logger.getLogger(UserController.class);
	@Autowired
	private Ms_tblUsuariosService ms_tblUsuariosService;

	@PostMapping(value = "/Autenticacion", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> Autenticacion(@RequestParam("Usuario") String Usuario,
			@RequestParam("UsuarioConexion") String UsuarioConexion,
			@RequestParam("ContraseniaConexion") String ContraseniaConexion) {
		// @Valid @RequestBody ObtenerServiciosRelacionadosDto datos
		// Validamos si existe el usuario
		User user = new User();
		Ms_tblUsuarios mstblusuario = ms_tblUsuariosService.getMs_tblUsuariobysLogin_id(UsuarioConexion);
		if (mstblusuario == null) {
			user.setCodigoRespuesta("1");
			user.setMensajeRespuesta("AUTENTICACION: PASSWORD O USUARIO INCORRECTO");
			user.setSesion("");
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		}
		if (!mstblusuario.getsPassword_id().equals(ContraseniaConexion)) {
			user.setCodigoRespuesta("1");
			user.setMensajeRespuesta("AUTENTICACION: PASSWORD O USUARIO INCORRECTO");
			user.setSesion("");
			return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
		}
		// Generamos el token
		String token = getJWTToken(Usuario);
		user.setCodigoRespuesta("0");
		user.setMensajeRespuesta("AUTENTICACION: PROCESO REALIZADO CON EXITO");
		user.setSesion(token);
		// return user;
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
		String token = Jwts.builder().setId("yattasoftJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();
		return "Bearer " + token;
	}
}
