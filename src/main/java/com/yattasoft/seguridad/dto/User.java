package com.yattasoft.seguridad.dto;

public class User {

	private String CodigoRespuesta;
	private String MensajeRespuesta;
	private String Sesion;
	public String getCodigoRespuesta() {
		return CodigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		CodigoRespuesta = codigoRespuesta;
	}
	public String getMensajeRespuesta() {
		return MensajeRespuesta;
	}
	public void setMensajeRespuesta(String mensajeRespuesta) {
		MensajeRespuesta = mensajeRespuesta;
	}
	public String getSesion() {
		return Sesion;
	}
	public void setSesion(String sesion) {
		Sesion = sesion;
	}

	
}
